#! /usr/bin/env node

import fs = require('fs');
import { MyServer } from './server/MyServer';

(() => {
  const PORT = Number(process.env.PORT) || 3000;
  const KEY = process.env.KEY;
  const CERT = process.env.CERT;

  if (!KEY) throw new Error();
  if (!CERT) throw new Error();

  const server = MyServer.getInstance(fs.readFileSync(KEY),
                                      fs.readFileSync(CERT));
  server.run(PORT);
})();
