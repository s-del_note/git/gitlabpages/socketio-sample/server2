export abstract class AbstractEventFactory {
  protected abstract createEvent(
    eventName: RequestEventName
  ): IServerEvent | null;

  readonly create = (eventName: RequestEventName): IServerEvent | null => {
    return this.createEvent(eventName);
  };
}
