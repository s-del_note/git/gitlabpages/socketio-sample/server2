declare type RequestEventName = 'connection'
                                | 'send_message'
                                | 'change_name'
                                | 'disconnect';

declare type ResponseEventName = 'no_secure_alert'
                                 | 'update_name'
                                 | 'response_message';
