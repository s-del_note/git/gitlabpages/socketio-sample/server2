import https = require('https');
import SocketIO = require('socket.io');
import { DefineEvents } from './event/DefineEvents';
import { User } from './user/User';

export class MyServer {
  private static instance: MyServer;
  private readonly server: https.Server;
  private readonly io: SocketIO.Server;
  private readonly userMap: Map<string, User>;

  /** Singleton */
  private constructor(key: Buffer, cert: Buffer) {
    this.server = https.createServer({ key, cert });
    this.io = new SocketIO.Server(
      this.server,
      {
        cors: { origin: 'https://s-del_note.gitlab.io' },
        cookie: false,
        serveClient: false
      }
    );
    this.userMap = new Map<string, User>();
    new DefineEvents(this.io, this.userMap).define();
  }

  static readonly getInstance = (
    key: Buffer,
    cert: Buffer
  ): MyServer => {
    if (!MyServer.instance) {
      MyServer.instance = new MyServer(key, cert);
    }
    return MyServer.instance;
  };

  readonly run = (port: number): void => {
    if (this.server.listening) return;
    this.server.listen(port);
    console.log(`MyServer is Listening: PORT => ${port}`);
  };
}
