export class UserName {
  private name: string;

  constructor() {
    this.name = '名無しさん';
  }

  readonly setName = (newName: string): void => {
    if (!newName) return;
    if (newName.length > 10) return;
    if (/^s+$/.test(newName)) return;

    this.name = newName;
  };

  readonly getName = (): string => this.name;
}
